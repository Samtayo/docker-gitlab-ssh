# Gitlab SSH Docker Image
This image makes it possible to commit to a Gitlab repository from within the Gitlab CI

## How to use
```yaml
update-dependencies:
    stage: ...
    image: technofab/docker-gitlab-ssh
    before_script:
        # start the ssh agent
        - eval "$(ssh-agent -s)"
        # add the private SSH key
        - echo "$GIT_SSH_PRIV_KEY" | tr -d '\r' | ssh-add - 
        # configure the "bot" user here. The email does not have to exist
        - git config --global user.email "hal9000@robot.ci"
        - git config --global user.name "HAL 9000"
        # check if everything worked
        - ssh git@gitlab.com || true
    script:
        - export CI_PUSH_REPO=`echo $CI_REPOSITORY_URL | sed -e "s|.*@\(.*\)|git@\1|" -e "s|/|:|"`
        - echo "do something here" >> todo.txt
        - git add .
        # [ci skip] is needed to not get an infinite loop of pipelines / alternatively use rules to prevent that
        - git commit -m "automatic update [ci skip]"
        # remove the old origin and add the right one where we can push to
        - git remote rm origin && git remote add origin ${CI_PUSH_REPO}
        - git push origin HEAD:${CI_BUILD_REF_NAME}
```

# How to get the SSH key
You can read about the whole process here, this image is just there to make your CI config a bit shorter:

https://about.gitlab.com/blog/2017/11/02/automating-boring-git-operations-gitlab-ci/
